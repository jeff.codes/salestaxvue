SalesTaxVue

Just a simple single page eCommerce application using Vue.js in order to showcase some basic code. 

The application will run from Visual Studio 2019 once opening the solution and pressing f5. 

Alternatively, you can use command prompt, navigate to the project folder and type "npm run serve" to run it locally. 