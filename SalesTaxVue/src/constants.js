export const constants = Object.freeze({
	SALES_TAX_RATE: 0.10,
	IMPORT_TAX_RATE: 0.05
})